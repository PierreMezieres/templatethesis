# Directory name
CH1=Ch1
CH2=Ch2
CH3=Ch3
CH4=Ch4
CH5=Ch5
CH6=Ch6
APP=appendix


CHS=$(CH1) $(CH2) $(CH3) $(CH4) $(CH5) $(CH6) $(APP) .

clean:
	for CH in $(CHS); do rm -f $$CH/*.aux $$CH/*.log $$CH/*.bbl $$CH/*.blg $$CH/*.toc $$CH/*.out $$CH/*.brf $$CH/*.bcf $$CH/*.fls $$CH/*.fdb_latexmk $$CH/*.lof $$CH/*.nav $$CH/*.snm $$CH/*.vrb; done

mrproper: clean 
	for CH in $(CHS); do rm -f $$CH/*.pdf; done

# Use at least once '$ make pdf' before using the other (c1, c2, c3, c4, c5, c6 or a)

pdf :
	@latexmk -pdf -jobname=thesis main.tex


# Compile each chapter independently

c1 :
	@latexmk -pdf -jobname=chapter1 -usepretex="\includeonly{$(CH1)/main}" main.tex
c2 :
	@latexmk -pdf -jobname=chapter2 -usepretex="\includeonly{$(CH2)/main}" main.tex
c3 :
	@latexmk -pdf -jobname=chapter3 -usepretex="\includeonly{$(CH3)/main}" main.tex
c4 :
	@latexmk -pdf -jobname=chapter4 -usepretex="\includeonly{$(CH4)/main}" main.tex
c5 :
	@latexmk -pdf -jobname=chapter5 -usepretex="\includeonly{$(CH5)/main}" main.tex
c6 :
	@latexmk -pdf -jobname=chapter6 -usepretex="\includeonly{$(CH6)/main}" main.tex
a :
	@latexmk -pdf -jobname=appendix -usepretex="\includeonly{$(APP)/appendix}" main.tex




# ----------------------------------------------------------------------------
# OLD COMMAND !
# ----------------------------------------------------------------------------

pdfA :
	@pdflatex --jobname=thesis main.tex

sli :
	@latexmk -pdf -jobname=slides slides.tex
ch1 :
	@latexmk -pdf -jobname=$(CH1)/chapter1 -usepretex="\includeonly{$(CH1)/main}" main.tex
ch2 :
	@latexmk -pdf -jobname=$(CH2)/chapter2 -usepretex="\includeonly{$(CH2)/main}" main.tex
ch3 :
	@latexmk -pdf -jobname=$(CH3)/chapter3 -usepretex="\includeonly{$(CH3)/main}" main.tex
ch4 :
	@latexmk -pdf -jobname=$(CH4)/chapter4 -usepretex="\includeonly{$(CH4)/main}" main.tex
ch5 :
	@latexmk -pdf -jobname=$(CH5)/chapter5 -usepretex="\includeonly{$(CH5)/main}" main.tex
ch6 :
	@latexmk -pdf -jobname=$(CH6)/chapter6 -usepretex="\includeonly{$(CH6)/main}" main.tex
app :
	@latexmk -pdf -jobname=$(APP)/appendix -usepretex="\includeonly{$(APP)/appendix}" main.tex

bib :
	bibtex $(CH1)/chapter1; \
	bibtex $(CH2)/chapter2; \
	bibtex $(CH3)/chapter3; \
	bibtex $(CH4)/chapter4; \
	bibtex $(CH5)/chapter5; \
	bibtex $(CH6)/chapter6; \
	bibtex thesis

cs: c1 c2 c3 c4 c5 c6
pdfs : pdf ch1 ch2 ch3 ch4 ch5 ch6
bibpdf: pdf bib
	make pdf; make pdf
pdf2:
	make pdf; make pdf
bibpdfs: pdfs bib
all : bib
	make pdfs; make pdfs
